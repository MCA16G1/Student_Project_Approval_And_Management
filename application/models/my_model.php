<?php
class my_model extends CI_model
{
	function can_login($user,$pass)
	{
		$this->db->where('Username',$user);
		$this->db->where('Password',$pass);

		$query=$this->db->get('Registration');

		if($query->num_rows()>0)
		{
			return $query->row()->Usertype;
		}
		else
		{
			return false;
		}
	}

	function insert($data)
	{
		$this->db->insert('Registration',$data);
	}

	function fetch_data()
	{
		$query=$this->db->get_where('Registration',array('Usertype'=>'student'));
		return $query;

	}
	function fetch_registered_data()
	{
		$query=$this->db->get_where('Registration',array('Usertype'=>'student','Approvedstatus'=>'approved'));
		return $query;
	}
	function guide_insert($data)
	{
		$this->db->insert('Staff',$data);
	}
	
}
?>