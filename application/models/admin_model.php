<?php
class admin_model extends CI_model
{
	function can_login($aduser,$adpass)
	{
		$this->db->where('Username',$aduser);
		$this->db->where('Password',$adpass);

		$query=$this->db->get('admin');

		if($query->num_rows()>0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	function fetch_data()
	{
		$query=$this->db->get_where('Registration',array('Usertype' => 'staff','Approvedstatus'=>'pending'));
		return $query;
	}

	function fetch_registered_data()
	{
		$query=$this->db->get_where('Registration',array('Usertype' => 'staff','Approvedstatus' => 'approved'));
		return $query;
	}
	function update($id)
	{
		$this->db->where('Rid',$id);
		$this->db->update('Registration',array('Approvedstatus'=>"approved"));
		return true;

	}
	function reject($id)
	{
		$this->db->where('Rid',$id);
		$this->db->update('Registration',array('Approvedstatus'=>"rejected"));
		return true;

	}
	function testing_insert($data)
	{
		$this->db->insert('Staff',$data);
	}
	
}
?>