<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Student Project Approval</title>
	<!-- core CSS -->
    <link href="<?php echo base_url();?>staffassets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>staffassets/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>staffassets/css/animate.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>staffassets/css/owl.carousel.css" rel="stylesheet">
    <link href="<?php echo base_url();?>staffassets/css/owl.transitions.css" rel="stylesheet">
    <link href="<?php echo base_url();?>staffassets/css/main.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
  
</head><!--/head-->

<body id="home" class="homepage">

    <header id="header">
        <nav id="main-menu" class="navbar navbar-default navbar-fixed-top top-nav-collapse" role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"><h2>Student Project Approval</h2></a>
                </div>
				
                <div class="collapse navbar-collapse navbar-right">
                    <ul class="nav navbar-nav">
                        <li ><a href="<?php echo base_url();?>staffhome">Home </a></li>
                        <li ><a href="#"></a></li>
                        <li class="active"><a href="<?php echo base_url();?>staff_stud_details">Student Details</a></li>
                        
                        <li><a href="<?php echo base_url();?>index">Logout</a></li>

                        <li class="scroll"><a href="#"></a></li>
                        <li class="scroll"><a href="#"></a></li>
                        <li class="scroll"><a href="#"></a></li>
                       <li class="scroll"><a href="#"></a></li>     
                         <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">LOGIN</button>                   
                    </ul>
                </div>
            </div><!--/.container-->
        </nav><!--/nav-->
    </header><!--/header-->

<div class="container">
  
  <!-- Trigger the modal with a button

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">LOGIN</h4>
        </div>
        <div class="modal-body">
          <form method="post" action="<?php echo base_url();?>welcome/staff_validation">
          <table>
          	<tr>
          		<td>UserName</td><td><input type="text" name="username"></td>
          	</tr>
          	<tr>
          		<td>PassWord</td><td><input type="password" name="pass"></td>
          	</tr>
          </table>

        
        <div class="modal-footer">
          <input type="submit" value="login">          
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </form>
      </div>
      
    </div>
  </div>
  
</div>

    <section id="main-slider">
        <div class="owl-carousel">
            <div class="item" style="background-image: url(<?php echo base_url();?>staffassets/images/bg1.jpg);">
                <div class="slider-inner">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <div class="carousel-content">
                                    <h2>The Art of Teaching is the Art of Assisting Discovery</h2>
                                    <p>One Child One Teacher One Book One Pen Can Change The World<br>--Malala--</p>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--/.item-->
             <div class="item" style="background-image: url(<?php echo base_url();?>staffassets/images/bg2.jpg);">
                <div class="slider-inner">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <div class="carousel-content">
                                    <h2>A Good Teacher Is A Determined Person</h2>
                                    <p>They Don't Remember What You Try To Teach Them. They Remember What You Are</p>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--/.item-->
        </div><!--/.owl-carousel-->
    </section><!--/#main-slider-->

    
<form method="post" action="<?php echo base_url() ?>welcome/insert">
<center>
  <?php if(isset($fetch_data))
      {
?>

   <table class="table">
      <tr>
        <th>Roll No.</th>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Year</th>
        <th>Email ID</th>
        <th>Topic</th>
        
      </tr>
      <?php
      if ($fetch_data->num_rows()>0) 
      {
             foreach ($fetch_data->result() as $row) 
             {
             ?>
             <tr>
                <td><?php echo $row->Rid; ?> </td>
                <td><?php echo $row->Fname; ?> </td>
                <td><?php echo $row->Lname; ?> </td>
                <td><?php echo $row->Year; ?> </td>
                <td><?php echo $row->Username; ?> </td>
                 <td><?php echo $row->AbstractFile; ?> </td>

             </tr>
             <?php
             }
        }
        else
        {
            ?>
            <tr>
                <td colspan=5> No Data Found </td>
            </tr>
            <?php
         }
         ?>
        </table>
      <?php } ?>
      </center>
    </form>
       

	




    <?php $this->load->view('footer')?>

    <script src="<?php echo base_url();?>staffassets/js/jquery.js"></script>
    <script src="<?php echo base_url();?>staffassets/js/bootstrap.min.js"></script>
    <!--<script src="http://maps.google.com/maps/api/js?sensor=true"></script>-->
    <script src="<?php echo base_url();?>staffassets/js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url();?>staffassets/js/mousescroll.js"></script>
    <script src="<?php echo base_url();?>staffassets/js/smoothscroll.js"></script>
    <script src="<?php echo base_url();?>staffassets/js/jquery.prettyPhoto.js"></script>
    <script src="<?php echo base_url();?>staffassets/js/jquery.isotope.min.js"></script>
    <script src="<?php echo base_url();?>staffassets/js/jquery.inview.min.js"></script>
    <script src="<?php echo base_url();?>staffassets/js/wow.min.js"></script>
    <script src="<?php echo base_url();?>staffassets/js/main.js"></script>
	<script src="<?php echo base_url();?>staffassets/js/scrolling-nav.js"></script>
<script>

    $(document).ready(function($) {
      $("#owl-example").owlCarousel();
    });

    $("body").data("page", "frontpage");

$("#owl-example").owlCarousel({ 
        items:3,   
/*        itemsDesktop : [1199,3],
        itemsDesktopSmall : [980,9],
        itemsTablet: [768,5],
        itemsTabletSmall: false,
        itemsMobile : [479,4]*/
})

    </script>