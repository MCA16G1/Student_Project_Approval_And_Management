
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Project Approval &mdash; 100% Free Fully Responsive HTML5 Template by FREEHTML5.co</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Free HTML5 Template by FREEHTML5.CO" />
	<meta name="keywords" content="free html5, free template, free bootstrap, html5, css3, mobile first, responsive" />
	<meta name="author" content="FREEHTML5.CO" />

  <!-- 
	//////////////////////////////////////////////////////

	FREE HTML5 TEMPLATE 
	DESIGNED & DEVELOPED by FREEHTML5.CO
		
	Website: 		http://freehtml5.co/
	Email: 			info@freehtml5.co
	Twitter: 		http://twitter.com/fh5co
	Facebook: 		https://www.facebook.com/fh5co

	//////////////////////////////////////////////////////
	 -->

  	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content=""/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content=""/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
	<link rel="shortcut icon" href="<?php echo base_url();?>homeassets/favicon.ico">

	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,300' rel='stylesheet' type='text/css'>
	
	<!-- Animate.css -->
	<link rel="stylesheet" href="<?php echo base_url();?>homeassets/css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="<?php echo base_url();?>homeassets/css/icomoon.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="<?php echo base_url();?>homeassets/css/bootstrap.css">
	<!-- Superfish -->
	<link rel="stylesheet" href="<?php echo base_url();?>homeassets/css/superfish.css">
	<!-- Magnific Popup -->
	<link rel="stylesheet" href="<?php echo base_url();?>homeassets/css/magnific-popup.css">
	<!-- Date Picker -->
	<link rel="stylesheet" href="<?php echo base_url();?>homeassets/css/bootstrap-datepicker.min.css">
	<!-- CS Select -->
	<link rel="stylesheet" href="<?php echo base_url();?>homeassets/css/cs-select.css">
	<link rel="stylesheet" href="<?php echo base_url();?>homeassets/css/cs-skin-border.css">
	
	<link rel="stylesheet" href="<?php echo base_url();?>homeassets/css/style.css">


	<!-- Modernizr JS -->
	<script src="js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

	</head>
	<body>
		<div id="fh5co-wrapper">
		<div id="fh5co-page">

		<header id="fh5co-header-section" class="sticky-banner">
			<div class="container">
				<div class="nav-header">
					<!--<a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle dark"><i></i></a>-->
					<center><h1 id="fh5co-logo"><a href="#">Student Project Approval</a></h1></center>
					<!-- START #fh5co-menu-wrap -->
					<nav id="fh5co-menu-wrap" role="navigation">
						<ul class="sf-menu" id="fh5co-primary-menu">
							<li class="active"><a href="#">Home</a></li>
							
							<li><a href="<?php echo base_url();?>guideprofile">Profile</a></li>
							<li><a href="<?php echo base_url();?>studentdetails">Student Details</a></li>
							<li><a href="<?php echo base_url();?>guidenotification">Notification</a></li>
						    <li><a href="<?php echo base_url();?>guidechangepassword">Change Password</a></li>
						    <li><a href="<?php echo base_url();?>staffhome">Logout</a></li>
							
						</ul>
					</nav>
				</div>
			</div>
		</header>

		<!-- end:header-top -->
	
		<div class="fh5co-hero">
			<div class="fh5co-overlay"></div>
			<div class="fh5co-cover" data-stellar-background-ratio="0.5" style="background-image: url(images/cover_bg_1.jpg);">
				<div class="desc">
					<div class="container">
							<div class="desc2 animate-box">
								<div class="col-sm-7 col-sm-push-1 col-md-7 col-md-push-1">
									<p><a href="http://frehtml5.co/" target="_blank" class="fh5co-site-name"></a></p>
									<h2>GuideHome</h2>
									<h3>Make your project unique and relevant</h3>
									<span class="price"></span>
									<!-- <p><a class="btn btn-primary btn-lg" href="#">Get Started</a></p> -->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
		
		<footer>
			<div id="footer">
				<div class="container">
					<div class="row row-bottom-padded-md">
						<div class="col-md-3 col-sm-3 col-xs-12 fh5co-footer-link">
							<h3>About </h3>
							<p>Student can submit maximum three topics with abstract before due date.
Faculty can access the details of the students and can perform keyword based search to check whether the topic is already exist or not.</p>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12 fh5co-footer-link">
							<h3>Top Digital Libraries</h3>
							<ul>
								<li><a href="#">IEEE Digital Library</a></li>
								<li><a href="#">Springer</a></li>
								<li><a href="#">Ibiblio</a></li>
								<li><a href="#">Google Books</a></li>
								<li><a href="#">Open Library</a></li>
							</ul>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12 fh5co-footer-link">
							<h3>Contact</h3>
							<ul>
								<li><a href="#">Dept of MCA, MES College of Engineering</a></li>
								<li><a href="#"></a></li>
								<li><a href="#"></a></li>
								<li><a href="#"></a></li>
							</ul>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12 fh5co-footer-link">
							<h3>Details</h3>
							<ul>
								<li><a href="#">syllabus</a></li>
								<li><a href="#"></a></li>
								<li><a href="#"></a></li>
								<li><a href="#"></a></li>
								<li><a href="#"></a></li>
							</ul>
						</div>
						
					
				</div>
			</div>
		</footer>

	

	</div>
	<!-- END fh5co-page -->

	</div>
	<!-- END fh5co-wrapper -->

	<!-- jQuery -->


	<script src="<?php echo base_url();?>homeassets/js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="<?php echo base_url();?>homeassets/js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="<?php echo base_url();?>homeassets/js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="<?php echo base_url();?>homeassets/js/jquery.waypoints.min.js"></script>
	<script src="<?php echo base_url();?>homeassets/js/sticky.js"></script>

	<!-- Stellar -->
	<script src="<?php echo base_url();?>homeassets/js/jquery.stellar.min.js"></script>
	<!-- Superfish -->
	<script src="<?php echo base_url();?>homeassets/js/hoverIntent.js"></script>
	<script src="<?php echo base_url();?>homeassets/js/superfish.js"></script>
	<!-- Magnific Popup -->
	<script src="<?php echo base_url();?>homeassets/js/jquery.magnific-popup.min.js"></script>
	<script src="<?php echo base_url();?>homeassets/js/magnific-popup-options.js"></script>
	<!-- Date Picker -->
	<script src="<?php echo base_url();?>homeassets/js/bootstrap-datepicker.min.js"></script>
	<!-- CS Select -->
	<script src="<?php echo base_url();?>homeassets/js/classie.js"></script>
	<script src="<?php echo base_url();?>homeassets/js/selectFx.js"></script>
	
	<!-- Main JS -->
	<script src="<?php echo base_url();?>homeassets/js/main.js"></script>

	</body>
</html>

