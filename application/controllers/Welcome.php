<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller 
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('index');
	}
	public function home()
	{
		$this->load->view('home');
	}
    public function approval_status()
	{
		$this->load->view('approval_status');
	}
	public function review_status()
	{
		$this->load->view('review_status');
	}
	public function profile()
	{
		$this->load->view('profile');
	}
	public function my_documents()
	{
		$this->load->view('my_documents');
	}
	public function abstract_submission()
	{
		$this->load->view('abstract_submission');
	}
	public function notification()
	{
		$this->load->view('notification');
	}
	public function change_password()
	{
		$this->load->view('change_password');
	}
	public function guidehome()
	{
		$this->load->view('guidehome');
	}
	public function registrationform()
	{
		$this->load->view('registrationform');
	}
	public function studform()
	{
		$this->load->view('studform');
	}
	public function guideprofile()
	{
		$this->load->view('guideprofile');
	}
	public function studentdetails()
	{
		$this->load->view('studentdetails');
	}
	public function guidenotification()
	{
		$this->load->view('guidenotification');
	}
	public function guidechangepassword()
	{
		$this->load->view('guidechangepassword');
	}
	public function staff_stud_details()
	{
		$this->load->model('my_model');
		$data["fetch_data"] = $this->my_model->fetch_registered_data();
		$this->load->view('staff_stud_details',$data);
	}
	public function coordinatorhome()
	{
		$this->load->view('coordinatorhome');
	}
	public function coordinatorprofile()
	{
		$this->load->view('coordinatorprofile');
	}
	public function co_guideallocation()
	{
		$this->load->view('co_guideallocation');
	}
	public function co_studentdetails()
	{
		$this->load->view('co_studentdetails');
	}
	public function co_staff_details()
	{
		$this->load->model('admin_model');
		$data["fetch_data"] = $this->admin_model->fetch_registered_data();
		$this->load->view('co_staff_details',$data);
	}
	public function co_notification()
	{
		$this->load->view('co_notification');
	}
	public function co_changepassword()
	{
		$this->load->view('co_changepassword');
	}
	public function adminlogin()
	{
		$this->load->view('adminlogin');
	}
	public function adminhome()
	{
		$this->load->view('adminhome');
	}
	public function co_pending_stud()
	{
		$this->load->model('my_model');
		$data["fetch_data"] = $this->my_model->fetch_data();
		$this->load->view('co_pending_stud',$data);
	}
	public function co_registered_stud()
	{
		$this->load->model('my_model');
		$data["fetch_data"] = $this->my_model->fetch_registered_data();
		$this->load->view('co_registered_stud',$data);
	}
	public function admin_pending_staff()
	{
		$this->load->model('admin_model');
		$data["fetch_data"] = $this->admin_model->fetch_data();
		$this->load->view('admin_pending_staff',$data);

	}
	public function admin_registered_staff()
	{
		$this->load->model('admin_model');
		$data["fetch_data"] = $this->admin_model->fetch_registered_data();
		$this->load->view('admin_registered_staff',$data);
	}
	public function admin_set_coordinator()
	{
		$this->load->model('admin_model');
		$data["fetch_data"] = $this->admin_model->fetch_registered_data();
		$this->load->view('admin_set_coordinator',$data);
	}
	public function staffhome()
	{
		$this->load->view('staffhome');
	}
	public function log_validation()
	{
		$this->form_validation->set_rules('username','username','required');
		$this->form_validation->set_rules('pass','password','required');
		if($this->form_validation->run())
		{
			$user=$this->input->post('username');
			$pass=$this->input->post('pass');

			$this->load->model('my_model');
			if($this->my_model->can_login($user,$pass) == "student")
			{
				$this->load->library('session');
				$session_data=array('username' => $user);
				$this->session->set_userdata($session_data);
				redirect(base_url().'welcome/home');
			}

			else if($this->my_model->can_login($user,$pass) == "staff")
			{
				$this->load->library('session');
				$session_data=array('username' => $user);
				$this->session->set_userdata($session_data);
				redirect(base_url().'welcome/staffhome');
			}

			else
			{
				redirect(base_url().'welcome/index');
				$this->session->set_flashdata('error','Invalid Username Or Password');
				redirect(base_url().'welcome/index');
			}
		}
		else
		{
			$this->index();
		}
	}

	public function insert()
	{
		$this->form_validation->set_rules('R_id','Registration_id','required');
		$this->form_validation->set_rules('F_name','F_name','required');
		$this->form_validation->set_rules('L_name','L_name','required');
		$this->form_validation->set_rules('Usertype','usertype','required');
		$this->form_validation->set_rules('Year','year','required');
		$this->form_validation->set_rules('Email','Email','required');
		$this->form_validation->set_rules('Pword','password','required');


		if($this->form_validation->run())
		{
			$this->load->model('my_model');
			$data=array('Rid' => $this->input->post('R_id'),
                        'Fname' => $this->input->post('F_name'),
                        'Lname' => $this->input->post('L_name'),
                        'Usertype' => $this->input->post('Usertype'),
                        'Year' => $this->input->post('Year'),
                        'Username' => $this->input->post('Email'),
                        'Password' => $this->input->post('Pword'));

			$this->my_model->insert($data);
			redirect(base_url().'welcome/index');		
		}

		else
		{
			$this->index();
		}
	}

	public function admin_validation()
	{
		$this->form_validation->set_rules('username','username','required');
		$this->form_validation->set_rules('pass','password','required');
		if($this->form_validation->run())
		{
			$aduser=$this->input->post('username');
			$adpass=$this->input->post('pass');
			$this->load->model('admin_model');

			if($this->admin_model->can_login($aduser,$adpass))
			{
				$this->load->library('session');
				$session_data=array('username' => $aduser);
				$this->session->set_userdata($session_data);
				redirect(base_url().'welcome/adminhome');
			}

			else
			{
				redirect(base_url().'welcome/adminlogin');
				$this->session->set_flashdata('error','Invalid Username Or Password');
				redirect(base_url().'welcome/adminlogin');
			}
		}
		
		else
		{
						$this->index();

			
		}

    }

public function staff_validation()
	{
		$this->form_validation->set_rules('username','username','required');
		$this->form_validation->set_rules('pass','password','required');
		if($this->form_validation->run())
		{

			
			$stuser=$this->input->post('username');
			$stpass=$this->input->post('pass');

			$this->load->model('staff_model');
			if($this->staff_model->can_login($stuser,$stpass) == "coordinator")
			{
				$this->load->library('session');
				$session_data=array('username' => $stuser);
				$this->session->set_userdata($session_data);
				redirect(base_url().'welcome/coordinatorhome');
			}

			else if($this->staff_model->can_login($stuser,$stpass) == "guide")
			{
				$this->load->library('session');
				$session_data=array('username' => $stuser);
				$this->session->set_userdata($session_data);
				redirect(base_url().'welcome/guidehome');
			}

			else
			{
				redirect(base_url().'welcome/staffhome');
				$this->session->set_flashdata('error','Invalid Username Or Password');
				redirect(base_url().'welcome/staffhome');
			}
		}
		else
		{
			
			$this->index();
		}
	}
 
public function update()
{
	$data=$this->uri->segment(3);
	$this->load->model('admin_model');
	$this->admin_model->update($data);
	redirect('admin_pending_staff');
}
public function reject()
{
	$data=$this->uri->segment(3);
	$this->load->model('admin_model');
	$this->admin_model->reject($data);
	redirect('admin_pending_staff');
}
/*public function co_set()
{
	$data=$this->uri->segment(3);
	$this->load->model('admin_model');
	$this->admin_model->co_set($data);
	redirect('admin_set_coordinator');
}*/
public function stu_update()
{
	$data=$this->uri->segment(3);
	$this->load->model('my_model');
	$this->admin_model->update($data);
	redirect('co_pending_stud');
}
public function stu_reject()
{
	$data=$this->uri->segment(3);
	$this->load->model('my_model');
	$this->admin_model->reject($data);
	redirect('co_pending_stud');
}
public function co_set()
{
	$data['segment']=$this->uri->segment(3);
	$this->load->view('admin_set_coordinator',$data);
}
public function guide_set()
{
	$data['segment']=$this->uri->segment(3);
	$this->load->view('co_staff_details',$data);
}
public function co_insert()
{
	$this->form_validation->set_rules('R_id','Registration_ID','required');
	$this->form_validation->set_rules('username','Username','required');
	$this->form_validation->set_rules('pass','Pass','required');
	$this->form_validation->set_rules('type','Type','required|is_unique[staff.Usertype]',array('is_unique'=> 'Coordinator Already Exist'));
	if($this->form_validation->run()=="true")
	{
		$this->load->model('admin_model');
		$data=array('Rid'=>$this->input->post('R_id'),
					'username' =>$this->input->post('username'),
					'password' =>$this->input->post('pass'),
					'Usertype'=>$this->input->post('type'));
		$this->admin_model->testing_insert($data);
		redirect(base_url().'admin_set_coordinator');
	}
	else
	{
		$data['segment']=$this->input->post('id');
		$this->load->view('admin_set_coordinator',$data);
	}
	
	
	

}
public function guide_insert()
{
	$this->form_validation->set_rules('R_id','Registration_ID','required');
	$this->form_validation->set_rules('username','Username','required');
	$this->form_validation->set_rules('pass','Pass','required');
	$this->form_validation->set_rules('type','Type','required');
	if($this->form_validation->run()=="true")
	{
		$this->load->model('admin_model');
		$data=array('Rid'=>$this->input->post('R_id'),
					'username' =>$this->input->post('username'),
					'password' =>$this->input->post('pass'),
					'Usertype'=>$this->input->post('type'));
		$this->my_model->guide_insert($data);
		redirect(base_url().'co_staff_details');
	}
	else
	{
		$data['segment']=$this->input->post('id');
		$this->load->view('co_staff_details',$data);
	}
	
	
	

}
}
